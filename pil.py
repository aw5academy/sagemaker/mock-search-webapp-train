import PIL
from PIL import Image

img = Image.open('out.png')
img = img.resize((240, 240), PIL.Image.ANTIALIAS)
img.save('out-240.png')
