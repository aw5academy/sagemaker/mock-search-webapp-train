#!/bin/bash

pip3 install pillow
wget "https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2" -O /tmp/phantomjs.tar.gz
tar xf /tmp/phantomjs.tar.gz -C /tmp/
sudo cp /tmp/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin/

NUM_FILES=100

rm -rf goodpics/
mkdir -p goodpics/
for (( i=1; i<=$NUM_FILES; i++ )); do
  phantomjs out.js
  python3 pil.py
  rm -f out.png
  uuid="`uuidgen`"
  mv out-240.png goodpics/${uuid}.png
done

rm -rf badpics/
mkdir -p badpics/
for (( i=1; i<=$NUM_FILES; i++ )); do
  phantomjs out.js bad
  python3 pil.py
  rm -f out.png
  uuid="`uuidgen`"
  mv out-240.png badpics/${uuid}.png
done
